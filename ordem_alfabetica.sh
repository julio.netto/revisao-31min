#!/bin/bash

read -p "Digite um nome " n1
read -p "Digite outro " n2

test -z "$n1" && echo "Nenhum nome foi dado."
test -z "$n2" && echo "O segundo nome não foi dado."

test "$n1" \< "$n2" && echo "$n1 - $n2" || echo "$n2 - $n1"

