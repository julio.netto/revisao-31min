#!/bin/bash

if [ "$#" -ne 5 ]; then
  echo "Erro: Fornecer exatamente 5 nomes de arquivos como argumentos."
  exit 1
fi

existentes=0
nao_existentes=0

for arquivo in "$@"; do
  if [ -e "$arquivo" ]; then
    existentes=$((existentes + 1))
  else
    nao_existentes=$((nao_existentes + 1))
  fi
done

echo "Quantidade de arquivos existentes: $existentes"
echo "Quantidade de arquivos não existentes: $nao_existentes"
