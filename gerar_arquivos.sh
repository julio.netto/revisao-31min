#!/bin/bash

generate_random_number() {
  echo $((RANDOM % 12 + 2))
}

random=$(generate_random_number)
random2=$(generate_random_number)
random3=$(generate_random_number)

while [ "$random" -eq "$random2" ] || [ "$random" -eq "$random3" ] || [ "$random2" -eq "$random3" ]; do
  random=$(generate_random_number)
  random2=$(generate_random_number)
  random3=$(generate_random_number)
done

arquivo1="$random.txt"
arquivo2="$random2.txt"
arquivo3="$random3.txt"

for arquivo in "$arquivo1" "$arquivo2" "$arquivo3"; do
  if [ -e "$arquivo" ]; then
    echo "Erro: Arquivo $arquivo já existe no diretório. Encerrando a execução."
    exit 1
  fi
done

touch "$arquivo1"
touch "$arquivo2"
touch "$arquivo3"

echo "Arquivos criados: $arquivo1, $arquivo2, $arquivo3."

